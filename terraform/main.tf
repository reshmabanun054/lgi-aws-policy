
locals{
yaml_files  = fileset("${path.root}/../scp-map/", "*.y*ml")
projects_decoded = [for file in local.yaml_files : yamldecode(file("${path.root}/../scp-map/${file}"))]
projects  = { for project in local.projects_decoded : project.scp.name => project.scp if project.scp.status == "active" }

}

module "cr_policy"{
  for_each = local.projects
  source = ".//module/ou-policy"

  #displayName = local.projects.displayName
  #jsonFile    = local.projects.jsonFile
  #targets  = local.projects.targets
  #namescp       = each.value.name
  displayName = each.value.displayName
  jsonFile    = each.value.jsonFile
  #targets     = each.value.targets
}

