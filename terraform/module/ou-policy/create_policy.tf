resource "aws_organizations_policy" "example" {
  name = var.displayName
  content = jsonencode(jsondecode(file("/builds/reshmabanun054/lgi-aws-policy/aws-scp/ou-policy/${var.jsonFile}")))
}