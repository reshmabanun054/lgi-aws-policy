variable "target_id" {
  default = "ou-z10r-t3g3v3tk"
  type = string
}

variable "file_path" {
  default = "/builds/reshmabanun054/lgi-aws-policy/aws-scp/ou-policy/policy-demo.json"
  type = string
}
variable "displayName"{
  type = string
}

variable "jsonFile"{
  type = string      
}
/*variable "targets"{
  type = string  
}*/