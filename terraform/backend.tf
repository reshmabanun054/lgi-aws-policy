terraform {
  backend "s3" {
    bucket         = "tf-st-file"
    key            = "Outputstate/terraform.tfstate"
    region         = "us-east-1"
  }
}